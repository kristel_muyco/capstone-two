let createCourse = document.querySelector("#createCourse")
let access = localStorage.getItem('token');

createCourse.addEventListener("submit", (e) => {
	e.preventDefault()

    let name = document.querySelector("#courseName").value
    let price = document.querySelector("#coursePrice").value
    let description = document.querySelector("#courseDescription").value


    fetch('https://whispering-refuge-64690.herokuapp.com/api/courses/add', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${access}`
            },

            body: JSON.stringify({
                name: name,
                price: price,
                description: description
            })
        })
        .then(res => {
            return res.json()
            
        })
        .then(data => {
            console.log(data)
                if(data === true){
                    Swal.fire({
                            icon: 'success',
                            text: 'Course Successfully Created',
                            customClass: 'Swal',
                            iconColor: 'Black',
                            confirmButtonColor: 'rgba(0,0,0, 0.7)',
                            confirmButtonText: '<a class="text-white text-decoration-none decoration-none" href="courses.html">Courses Page</a>',
                            showClass: {
                                popup: 'animate__animated animate__fadeInDown'
                            },
                            hideClass: {
                                popup: 'animate__animated animate__fadeOutUp'
                                }
                        
                        })
                    } else {
                       Swal.fire({
                            icon: 'error',
                            text: 'Please Check Course Details',
                            customClass: 'Swal',
                            iconColor: 'Black',
                            confirmButtonColor: 'rgba(0,0,0, 0.7)',
                            showClass: {
                                popup: 'animate__animated animate__fadeInDown'
                            },
                            hideClass: {
                                popup: 'animate__animated animate__fadeOutUp'
                                }
                        
                        })                      
                    }
        })
    })