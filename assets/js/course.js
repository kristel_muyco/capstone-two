let coursePage = document.querySelector("#coursePage")
let enrolleesPage = document.querySelector("#enrolleesPage")
let access = localStorage.getItem('token');


let parameter = window.location.search;
const urlId = new URLSearchParams(parameter);
let id = urlId.get('id');


    fetch(`https://whispering-refuge-64690.herokuapp.com/api/users/details`, {
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${access}`,
        }
    })
    .then(res =>res.json())
    .then(data => {

        if(data.isAdmin === false){
            const enrollBtn = document.querySelector('#enrollBtn');

            const courseId= document.querySelector('#courseId');
            const name= document.querySelector('#courseName');
            const desc= document.querySelector('#courseDesc');
            const price= document.querySelector('#coursePrice');


            fetch(`https://whispering-refuge-64690.herokuapp.com/api/courses/${id}`, {
                    headers: {
                        'Authorization': `Bearer ${access}`
                    }
            })
                .then(res => res.json())
                .then(data => {

                    courseId.innerText = `Course ID: ${data._id}` 
                    name.innerText = `${data.name}`
                    desc.innerText = `${data.description}`
                    price.innerText = `₱ ${data.price}`
                })
                //enroll function
                enrollBtn.addEventListener("click", (e) => {
                    e.preventDefault()

                    const courseId = document.querySelector('#inputId').value

                    if(courseId !== ""){
                        fetch(`https://whispering-refuge-64690.herokuapp.com/api/users/enroll`, {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json',
                            'Authorization': `Bearer ${access}`,
                        },

                        body: JSON.stringify({
                            courseId: courseId
                        })

                    })  
                    .then(res => res.json())
                    .then(data => {
                       Swal.fire({
                            icon: 'success',
                            text: 'Enrollment Successful!',
                            customClass: 'Swal',
                            iconColor: 'Black',
                            confirmButtonColor: 'rgba(0,0,0, 0.7)',
                            confirmButtonText: '<a class="text-white text-decoration-none decoration-none" href="profile.html">My Profile</a>',
                            showClass: {
                                popup: 'animate__animated animate__fadeInDown'
                            },
                            hideClass: {
                                popup: 'animate__animated animate__fadeOutUp'
                                }
                        
                        })
                      
                    })

                    }else {
                       Swal.fire({
                            icon: 'error',
                            text: 'Please Check Course ID',
                            customClass: 'Swal',
                            iconColor: 'Black',
                            confirmButtonColor: 'rgba(0,0,0, 0.7)',
                            showClass: {
                                popup: 'animate__animated animate__fadeInDown'
                            },
                            hideClass: {
                                popup: 'animate__animated animate__fadeOutUp'
                                }
                        
                        })                 
                    }
                }) 

        }else{
             fetch(`https://whispering-refuge-64690.herokuapp.com/api/courses/${id}`, {
                    method: 'GET',
                    headers: {
                        'Content-Type': 'application/json',
                        Authorization: `Bearer ${access}`,
                    }
            })
            .then(res => res.json())
            .then(data => {
                
            coursePage.innerHTML = 
                `

                        <h2 class="display-4 mb-3">COURSE DETAILS</h2>
                        <div class="card">
                            <div class="card-header ml-auto" id="adminButtons">
                                <a href="editCourse.html?id=${data._id}" class="btn btn-md btn-light onclick">
                                     <i class="fa fa-edit"></i>
                                </a>
                                <a href="deleteCourse.html?id=${data._id}" class="btn btn-md btn-light onclick">
                                      <i class="fa fa-calendar-times"></i>
                                </a>
                            </div>

                            <div class="card-body">
                                <h2 id="courseName" class="mt-5">${data.name}</h2>
                                <p><small><emp>Course Name</emp></small></p>
                                <br>
                                <p id="courseDesc" class="lead">${data.description}</p>
                                <p class="secondary">Price: 
                                    <span id="coursePrice">₱ ${data.price}</span>
                                </p>
                            </div>

                            <div class="card-footer ml-auto custom-control custom-switch">
                                  <label class="custom-control-label" for="customSwitch1" id="toggleLabel"></label>
                                  <input 
                                    type="checkbox" 
                                    data-toggle="toggle"
                                    data-onstyle="light" 
                                    data-offstyle="dark"
                                    class="custom-control-input" 
                                    id="customSwitch1"  
                                    checked>
                                    
                            </div> 
                        </div>

                `

                //Active/Archive
                let toggle =  document.querySelector("#customSwitch1")
                let toggleLabel = document.getElementById("toggleLabel")
                        
                let parameter = window.location.search;
                const urlId = new URLSearchParams(parameter);
                let id = urlId.get('id');
                        

                toggle.addEventListener("change", (e) =>{
                    if(toggle.checked === true){
                        fetch(`https://whispering-refuge-64690.herokuapp.com/api/courses/${id}`, {
                            method: 'PUT',
                            headers: {
                                'Content-Type': "application/json",
                                Authorization: `Bearer ${access}`,
                            },
                            
                            body: JSON.stringify({
                                isActive: true
                            })
                        })
                        .then(res=> res.json())
                        toggleLabel.innerHTML = "Active"                              

                    }else{
                        fetch(`https://whispering-refuge-64690.herokuapp.com/api/courses/${id}`, {
                            method: 'PUT',
                            headers: {
                                'Content-Type': "application/json",
                                Authorization: `Bearer ${access}`,
                            },
                        })
                        .then(res=> res.json())
                        toggleLabel.innerHTML = "Inactive"                                 
                    }
                })


    
            enrolleesPage.innerHTML = 
                        `
                        <section class="card jumbotron">
                            <table class="table table-hover">
                                <h3 class="text-center text-light">
                                    Enrolled Students
                                </h3>
                                <thead>
                                    <tr>
                                        <th class= "text-light"> First Name </th>
                                        <th class= "text-light"> Last Name </th>
                                    </tr>
                                    <tbody id="enrollees">
                                    </tbody>
                                </thead>
                            </table>
                        </section>
                        `
            let enrollees;
            for(let i = 0; i< data.enrollees.length; i++){
                fetch(`https://whispering-refuge-64690.herokuapp.com/api/users/${data.enrollees[i].userId}`, {
                    headers: {
                        Authorization: `Bearer ${access}`,
                    },
                })
                .then(res=> res.json())
                .then(course => {
                    if(enrollees){
                        enrollees = 
                            `${enrollees}
                                <tr>
                                    <td class= "text-light">${course.firstName}</td>
                                    <td class= "text-light">${course.lastName}</td>
                                </tr>

                            `
                    }else{
                        enrollees = 
                            `
                                <tr>
                                    <td class= "text-light">${course.firstName}</td>
                                    <td class= "text-light">${course.lastName}</td>
                                </tr>

                            `
                    }
                    if(enrollees){
                        document.querySelector("#enrollees").innerHTML = enrollees;
                    }
                })
            }
            
            })
      
            }

    })
