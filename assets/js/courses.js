let access = localStorage.getItem('token');
let adminButtons = document.querySelector("#adminButtons")
let userButton = document.querySelector("#userButton")
console.log(access)




    // admin/User buttons
if(access){
    fetch(`https://whispering-refuge-64690.herokuapp.com/api/users/details`, {
        headers: {
            Authorization: `Bearer ${access}`
        }
     })
     .then(res => res.json())
     .then(data => {
     
    if(data.isAdmin === true){
                    adminButtons.innerHTML = 
                    `
                            <button class="ml-3 card btn btn-circle">
                                <a href="./addCourse.html" class="btn btn-md btn-light">
                                    <i class="fa fa-plus"></i>
                                </a>
                            </button>

                    `
    const get = fetch(`https://whispering-refuge-64690.herokuapp.com/api/courses/all`, {
        headers: {
        Authorization: `Bearer ${access}`
        }

    })


        get.then(res => res.json())
        .then(data => {
    let courses;
    for(let i = 0; i< data.length; i++){

    console.log(data)
        if(courses){
            courses = `${courses}
            <div class="col-md-4 my-3">
                <div class="card">
                    <div class="card-header" id="adminButtons">
                        <a href="editCourse.html?id=${data[i]._id}" class="btn btn-md btn-light onclick">
                             <i class="fa fa-edit"></i>
                        </a>
                        <a href="deleteCourse.html?id=${data[i]._id}" class="btn btn-md btn-light onclick">
                              <i class="fa fa-calendar-times"></i>
                        </a>
                    </div>
                    <div class="card-body">
                        <h2 class="card-title"> ${data[i].name}</h2>
                        <p class="card-title"> ${data[i].description}</p>
                        <p class="card-text">Price: ₱${data[i].price}</p>
                        <a href="course.html?id=${data[i]._id}" class="btn btn-sm btn-light">Course Details</a>
                    </div>
                    <div class="card-footer" id="userButton">
                      
                    </div>

                </div>
            </div>`
        }else{
            courses = `<div class="col-md-4 my-3">
                <div class="card">
                    <div class="card-header" id="adminButtons">
                        <a href="editCourse.html?id=${data[i]._id}" class="btn btn-md btn-light onclick">
                             <i class="fa fa-edit"></i>
                        </a>
                        <a href="deleteCourse.html?id=${data[i]._id}" class="btn btn-md btn-light onclick">
                              <i class="fa fa-calendar-times"></i>
                        </a>
                    </div>
                    <div class="card-body">
                        <h2 class="card-title"> ${data[i].name}</h2>
                        <p class="card-title"> ${data[i].description}</p>
                        <p class="card-text">Price: ₱${data[i].price}</p>
                        <a href="course.html?id=${data[i]._id}" class="btn btn-sm btn-light">Course Details</a>
                    </div>
                    <div class="card-footer" id="userButton">
                       
                    </div>

                </div>
            </div>`
        }

        if(courses){
            document.querySelector("#courseContainer").innerHTML = courses;
        }
    } 

 })
}else {
    const get = fetch(`https://whispering-refuge-64690.herokuapp.com/api/courses/all`)


         get.then(res => res.json())
         .then(data => {
            console.log(data)
    let courses;
    for(let i = 0; i< data.length; i++){
        if(courses){
            courses = `${courses}
            <div class="col-md-4 my-3">
                <div class="card">
                    <div class="card-header" id="adminButtons">
                    </div>
                    <div class="card-body">
                        <h2 class="card-title"> ${data[i].name}</h2>
                        <p class="card-title"> ${data[i].description}</p>
                        <p class="card-text">Price: ${data[i].price}</p>
                    </div>
                    <div class="card-footer" id="userButton">
                        <a href="./course.html?id=${data[i]._id}"  class="btn btn-sm btn-light">
                            Class Details
                        </a>
                    </div>
                </div>
            </div>`
        }else{
            courses = `<div class="col-md-4 my-3">
                <div class="card">
                    <div class="card-header" id="adminButtons">
                    </div>
                    <div class="card-body">
                        <h2 class="card-title"> ${data[i].name}</h2>
                        <p class="card-title"> ${data[i].description}</p>
                        <p class="card-text">Price: ${data[i].price}</p>
                    </div>
                    <div class="card-footer" id="userButton">
                        <a href="./course.html?id=${data[i]._id}" class="btn btn-sm btn-light">
                            Class Details
                        </a>
                    </div>
                </div>
            </div>`
        }
        if(courses){
            document.querySelector("#courseContainer").innerHTML = courses;
        }
    } 
    
})

}
})
}
