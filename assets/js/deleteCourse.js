let deleteCourse = document.querySelector("#deleteCourse")
let access = localStorage.getItem('token');

let parameter = window.location.search;
const urlId = new URLSearchParams(parameter);
let id = urlId.get('id');

    if(access){

        fetch(`https://whispering-refuge-64690.herokuapp.com/api/courses/${id}`, {
    	method: 'DELETE',
    	headers: {
    		'Content-Type': 'application/json',
            Authorization: `Bearer ${access}`
    	},

    })
    .then(res => {
    	return res.json()
    })
    .then(data => {
    	console.log(data)
    		if(data === true){
                (data.isActive = false)
                    Swal.fire({
                            icon: 'success',
                            text: 'Course Successfully Deleted',
                            customClass: 'Swal',
                            iconColor: 'Black',
                            confirmButtonColor: 'rgba(0,0,0, 0.7)',
                            confirmButtonText: '<a class="text-white text-decoration-none decoration-none" href="courses.html">Courses Page</a>',
                            showClass: {
                                popup: 'animate__animated animate__fadeInDown'
                            },
                            hideClass: {
                                popup: 'animate__animated animate__fadeOutUp'
                                }
                        
                        })
    		} else {
                      Swal.fire({
                            icon: 'error',
                            text: 'Please Check Course Details',
                            customClass: 'Swal',
                            iconColor: 'Black',
                            confirmButtonColor: 'rgba(0,0,0, 0.7)',
                            showClass: {
                                popup: 'animate__animated animate__fadeInDown'
                            },
                            hideClass: {
                                popup: 'animate__animated animate__fadeOutUp'
                                }
                        
                        })  
    		}
    })
}