let loginForm = document.querySelector("#loginUser")


loginForm.addEventListener("submit", (e) => {
    e.preventDefault()

    let email = document.querySelector("#email").value
    let password  = document.querySelector("#pwd").value   

    if(email == "" || password == "") {
        alert("Please input your email and/or password")
    } 
    else {
        fetch('https://whispering-refuge-64690.herokuapp.com/api/users/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            }, 
            body: JSON.stringify({
               email: email,
               password: password 
            })
        })
        .then(res => {
            return res.json()
        })
        .then(data => {
            console.log(data)
            if(data.access){
                localStorage.setItem('token', data.access); 
                
                fetch(`https://whispering-refuge-64690.herokuapp.com/api/users/details`, {
                    headers: {
                        Authorization: `Bearer ${data.access}`
                    }
                })
                .then(res => {
                    return res.json()
                })
                .then(data => {
                    localStorage.setItem("id", data._id)
                    localStorage.setItem("isAdmin", data.isAdmin)
                    localStorage.setItem("firstName", data.firstName)

                    if(data.isAdmin === true){
                        window.location.replace("./courses.html")
                    }else{
                        window.location.replace("./profile.html")
                    }
                    
                })
            } else {
                       Swal.fire({
                            icon: 'error',
                            text: 'Please Check Credentials',
                            customClass: 'Swal',
                            iconColor: 'Black',
                            confirmButtonColor: 'rgba(0,0,0, 0.7)',
                            showClass: {
                                popup: 'animate__animated animate__fadeInDown'
                            },
                            hideClass: {
                                popup: 'animate__animated animate__fadeOutUp'
                                }
                        
                        }) 
            }
        })
    }

})