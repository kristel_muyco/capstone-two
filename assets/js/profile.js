let userProfile = document.querySelector("#profileCard")
let access = localStorage.getItem('token');

let id = localStorage.getItem('id');
let enrolledOn = localStorage.getItem('enrolledOn');
let status = localStorage.getItem('status');
console.log(access)

 const get = fetch(`https://whispering-refuge-64690.herokuapp.com/api/users/details`, {
    method: "GET",
    headers: {
        Authorization: `Bearer ${access}`
    }
 })
 get.then(res => res.json())
 .then(data => {
    console.log(data)
        userProfile.innerHTML = 
            `
                        <div class="card">
                            <div class="row d-block img-div justify-content-center">
                                <img class="img" src=".././assets/images/profile.svg" alt="profilePic">
                            </div>
                            
                            <div class="card-header ml-auto px-5" id="userButtons">
                                <a href="#" class="btn btn-md btn-light onclick">
                                        <i class="fa fa-edit"></i>
                                </a>
                            </div>
                            <div class="card-body px-5">
                                <h4 class="text-left mt-5">Name: ${data.firstName} <span>${data.lastName}</span</h4>
                                <h4 class="text-left mt-5">Email Address: ${data.email}</h4>
                                <h4 class="text-left mt-5">Mobile No: ${data.mobileNo}</h4>

                            </div>
                            <div class="card-footer ml-auto px-5" id="userButtons">
                            </div>
                        </div>
            `

    let enrollments;
    for(let i = 0; i< data.enrollments.length; i++){
        fetch(`https://whispering-refuge-64690.herokuapp.com/api/courses/${data.enrollments[i].courseId}`, {
            method: "GET",
            headers: {
                Authorization: `Bearer ${access}`
            }
        })
        .then(res => res.json())
        .then(course => {
        
            if(enrollments){ 
                enrollments = 
                        `${enrollments}
                        <tr class="justify-content-center d-flex">
                            <td class="text-light">${course.name}</td>
                            <td class="text-light">${data.enrollments[i].enrolledOn} </td>
                            <td class="text-light">${data.enrollments[i].status} </td>            
                        </tr>
                        `
            }else{
                enrollments = 
                    `
                    <tr class="justify-content-center d-flex">
                        <td class="text-light">${course.name}</td>
                        <td class="text-light">${data.enrollments[i].enrolledOn} </td>
                        <td class="text-light">${data.enrollments[i].status} </td>            
                    </tr>
                    `
            }
            if(enrollments){
                document.querySelector('#enrollments').innerHTML = enrollments;
            }
        })
    }

 })