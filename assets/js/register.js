let registerForm = document.querySelector("#registerUser")

registerForm.addEventListener("submit", (e) => {
    e.preventDefault()
    
    let firstName = document.querySelector("#fName").value
    let lastName = document.querySelector("#lName").value 
    let mobileNo = document.querySelector("#mobile").value
    let email = document.querySelector("#email").value
    let password1 = document.querySelector("#pwd").value
    let password2 = document.querySelector("#pwd2").value


    //lets create a validation to enable the submit button when all fields are populated and both passwords should match otherwise it should not continue.
    if((password1 !== '' && password2 !== '') && (password2 === password1) && (mobileNo.length === 11)) {

        //if all the requirements are met, then now lets check for duplicate emails in the database first. 
        fetch('https://whispering-refuge-64690.herokuapp.com/api/users/email-exists', {
            method: 'POST', 
            headers: {
                'Content-Type': 'application/json'
            }, 
            body: JSON.stringify({
                email: email,
            })
        })
        .then(res => res.json())
        .then(data => {
            //if no email duplicates are found 
            if(data === false){
                fetch('https://whispering-refuge-64690.herokuapp.com/api/users', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        firstName: firstName,
                        lastName: lastName,
                        email : email,
                        mobileNo: mobileNo,
                        password: password1
                    })
                })
                .then(res => {
                    return res.json()
                })
                .then(data => {
                    console.log(data)
                    //lets give a proper response if registration will become successful
                    if(data === true){
                    Swal.fire({
                            icon: 'success',
                            text: 'New Account Created!',
                            customClass: 'Swal',
                            iconColor: 'Black',
                            confirmButtonColor: 'rgba(0,0,0, 0.7)',
                            confirmButtonText: '<a class="text-white text-decoration-none decoration-none" href="login.html">Log in</a>',
                            showClass: {
                                popup: 'animate__animated animate__fadeInDown'
                            },
                            hideClass: {
                                popup: 'animate__animated animate__fadeOutUp'
                                }
                        
                        })
                    } else {
                       Swal.fire({
                            icon: 'error',
                            text: 'Something Went Wrong in Registration!',
                            customClass: 'Swal',
                            iconColor: 'Black',
                            confirmButtonColor: 'rgba(0,0,0, 0.7)',
                            showClass: {
                                popup: 'animate__animated animate__fadeInDown'
                            },
                            hideClass: {
                                popup: 'animate__animated animate__fadeOutUp'
                                }
                        
                        })  
                    }
                })
            }
        })
    } else {
                       Swal.fire({
                            icon: 'error',
                            text: 'Please Check your Credentials!',
                            customClass: 'Swal',
                            iconColor: 'Black',
                            confirmButtonColor: 'rgba(0,0,0, 0.7)',
                            showClass: {
                                popup: 'animate__animated animate__fadeInDown'
                            },
                            hideClass: {
                                popup: 'animate__animated animate__fadeOutUp'
                                }
                        
                        })  
    }
})
