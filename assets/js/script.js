let navItem1 =  document.querySelector("#navSession1") 
let navItem2 =  document.querySelector("#navSession2") 
let userToken = localStorage.getItem("token")
let firstName = localStorage.getItem("firstName")

  if(!userToken) {
	navItem1.innerHTML = 
				`
					<li class="nav-item">
						<a href="./login.html" class="nav-link text-dark"> Log in </a>
					</li>

				`
	navItem2.innerHTML = 
				`
					<li class="nav-item">
						<a href="./register.html" class="nav-link text-dark"> Register </a>
					</li>

				`
} else {
	navItem1.innerHTML = 
	`
				<li class="nav-item">
					<a href="./logout.html" class="nav-link text-dark">Log Out </a>
				</li>				
	`
	navItem2.innerHTML = 
	`
				<li class="nav-item">
					<a href="./profile.html" class="nav-link text-dark"><strong>  Hello, ${firstName}!</strong>  </a>
				</li>
	`

}

if(userToken){
    fetch(`https://whispering-refuge-64690.herokuapp.com/api/users/details`, {
        headers: {
            Authorization: `Bearer ${userToken}`
        }
     })
     .then(res => res.json())
     .then(data => {
     
    if(data.isAdmin === true && userToken){
		navItem2.innerHTML = 
		`
					<li class="nav-item d-none">
						<a href="./profile.html" class="nav-link text-dark"> Hello, ${firstName}! </a>
					</li>				
		`    	
    }
   })
}


